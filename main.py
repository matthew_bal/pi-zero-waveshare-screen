import epd2in7
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from datetime import datetime
import RPi.GPIO as GPIO
import time

from youtube import get_subs

# General way I'll make this work:
# Cronjobs... every 15s it will run this main python script.

# First, we need to connect to the internet and get subscriber data
# Then, we generate the BMP image to display on the pi.

# We then save this image and compare it to the last rendered image.
# If the image is different, we need to update the display

# Update the display

# Once development is done, I will also have it git pull every 30 minutes.
# For pull requests by anonymous users, gitlab rate liits to 100 pull requests per six hours;
# this is one pull every 3.6 minutes. Lets do 30 minutes to be nicer

# We might want to change this later, to handle partial screen refreshes if possible...
# We might even want to add the ability to interact with the buttons! That could be a whole other thing perhaps?

def buttonChecks():
    EPD_KEY = (5, 6, 13, 19)
    GPIO.setmode(GPIO.BCM) # See BCM GPIO mapping
    for i in range(4):
        GPIO.setup(EPD_KEY[0], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    while True:
        if GPIO.input(EPD_KEY[0]) == GPIO.LOW:
            print('Button pressed')
            break
        time.sleep(0.01)


def generateNextFrame():

    image = Image.new('1', (epd2in7.EPD_HEIGHT, epd2in7.EPD_WIDTH), 255)    # 255: clear the image with white
    image = Image.open('saskalogo.bmp')

    draw = ImageDraw.Draw(image)

    font = ImageFont.truetype('Roboto-Bold.ttf', 64)
    smallFont = ImageFont.truetype('Roboto-Bold.ttf', 16)

    subscribers = get_subs()

    # For each letter less, add 10 horizontal?

    offset = 23

    digitCount = len(subscribers)
    diff = 6 - digitCount

    extraOffset = diff * 18

    offset += extraOffset

    draw.text((offset, 5), subscribers, font = font, fill = 0)
    draw.text((90, 80), "subscribers", font = smallFont, fill = 0)

    draw.text((40, 130), "SASKA", font = smallFont, fill = 0)
    draw.text((175, 130), "LILY", font = smallFont, fill = 0)

    image = image.rotate(90, expand=True)

    image.save("nextFrame.bmp")

def isNewFrame():
    # return true if next frame matches old frame

    nextData = Image.open('nextFrame.bmp')

    lastData = Image.open('lastFrame.bmp')

    if lastData == nextData:
        return False
    
    return True

def replaceOldFrame():
    nextData = Image.open('nextFrame.bmp')
    nextData.save("lastFrame.bmp")

def drawFrame():
    epd = epd2in7.EPD()
    epd.init()
    epd.display_frame(epd.get_frame_buffer(Image.open('nextFrame.bmp')))


# def main():
#     epd = epd2in7.EPD()
#     epd.init()

#     # For simplicity, the arguments are explicit numerical coordinates
#     image = Image.new('1', (epd2in7.EPD_WIDTH, epd2in7.EPD_HEIGHT), 255)    # 255: clear the image with white
#     draw = ImageDraw.Draw(image)
#     font = ImageFont.truetype('FreeMonoBold.ttf', 18)



#     draw.text((20, 50), 'Testing...', font = font, fill = 0)
#     draw.rectangle((0, 76, 176, 96), fill = 0)
#     # draw.text((18, 80), 'Hello world!', font = font, fill = 255)
#     # draw.line((10, 130, 10, 180), fill = 0)
#     # draw.line((10, 130, 50, 130), fill = 0)
#     # draw.line((50, 130, 50, 180), fill = 0)
#     # draw.line((10, 180, 50, 180), fill = 0)
#     # draw.line((10, 130, 50, 180), fill = 0)
#     # draw.line((50, 130, 10, 180), fill = 0)
#     # draw.arc((90, 190, 150, 250), 0, 360, fill = 0)
#     # draw.chord((90, 120, 150, 180), 0, 360, fill = 0)
#     # draw.rectangle((10, 200, 50, 250), fill = 0)

#     # epd.display_frame(epd.get_frame_buffer(image))

#     # display images
#     epd.display_frame(epd.get_frame_buffer(Image.open('test.bmp')))

if __name__ == '__main__':

    print("Starting...")

    print("Generating frame")
    # Generate next frame
    generateNextFrame()

    print("Checking frame")
    # if new frame, would draw

    shouldDraw = isNewFrame()

    if shouldDraw:
        print("Drawing new frame...")
        drawFrame()
        print("Overriding last frame bmp...")
        # Now replace the old frame
        replaceOldFrame()
    