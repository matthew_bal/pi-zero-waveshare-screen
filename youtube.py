
import os
from dotenv import load_dotenv
from requests import get
load_dotenv()


def get_subs():
    channelGetURL = "https://content-youtube.googleapis.com/youtube/v3/channels?part=statistics&id={}&key={}".format(os.getenv('CHANNEL_ID'), os.getenv('YOUTUBE_API_KEY'))
    print(channelGetURL)

    response = get(channelGetURL)
    data = response.json()

    return data['items'][0]['statistics']['subscriberCount']

if __name__ == "__main__":
    print("Testing youtube subs")
    print(os.getenv('YOUTUBE_API_KEY'))

    # Get it..
    print(get_subs())



